# SimplyNews

SimplyNews is a website to read articles from other sites. Without JavaScript, ads or any other interruptions. Just content.

SimplyNews aims to be text-browser compatible while also looking very clean on ordinary browsers.

### TODOs
- [ ] i18n
- [ ] Support for sending patches via email
